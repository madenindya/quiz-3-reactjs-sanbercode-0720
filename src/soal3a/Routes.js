import React from "react";
import { Switch, Route } from "react-router";

import ArticleIndex from '../soal2/ArticleIndex';
import About from "../soal3b/About";
import Home from "../soal4/Home"
import Movie from "../soal5/Movie"


const Routes = () => {

  return (
    <div>
      <Switch>
        <Route exact path="/">
          <ArticleIndex />
        </Route>
        <Route path="/home">
          <Home />
        </Route>
        <Route path="/about">
          <About />
        </Route>
        <Route path="/movies">
          <Movie />
        </Route>
      </Switch>
    </div>
  );
};

export default Routes;
