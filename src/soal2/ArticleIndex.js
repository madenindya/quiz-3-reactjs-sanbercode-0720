import React  from 'react';

class ArticleItem extends React.Component {
    render() {
        return (
            <div>
                <a href="/"><h3>Lorem Post {this.props.idx}</h3></a>
                <p>
                    Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut. Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
                </p>
                <hr />
            </div>
        )
    }
}

class ArticleContent extends React.Component {
    render() {
        return (
            <>
            {
                [1,2,3,4,5,6,7,8,9,10].map((value) => {
                    return (
                        <ArticleItem key={value} idx={value} />
                    )
                })
            }
            </>
        )
    }
}

class ArticleFooter extends React.Component {
    render() {
        return (
        <footer>
            <h5>copyright &copy; 2020 by Sanbercode</h5>
        </footer>
        )
    }
}

class ArticleIndex extends  React.Component {
    render() {
        return (
            <>
                <section className="col-75 margin-center main-content">
                    <h1>Featured Posts</h1>
                    <ArticleContent />
                </section>
                <ArticleFooter />
            </>
        )
    }
}


export default ArticleIndex
