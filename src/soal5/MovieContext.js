import React, { useState, createContext, useEffect } from "react";
import axios from 'axios';

export const MovieContext = createContext();

export const MovieProvider = props => {
  const [movies, setMovies] = useState([]);
  const [inputTitle, setInputTitle] = useState("");
  const [inputDescription, setInputDescription] = useState("");
  const [inputYear, setInputYear] = useState(0);
  const [inputDuration, setInputDuration] = useState(0);
  const [inputGenre, setInputGenre] = useState("");
  const [inputRating, setInputRating] = useState(1);
  const [indexOfForm, setIndexOfForm] =  useState(-1)
  const [idOfList, setIdOfList] =  useState(-1)

  useEffect(() => {
    if (movies.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setMovies(newData);
        })
    }
  })

  return (
    <MovieContext.Provider value={
        [
          movies, setMovies,
          inputTitle, setInputTitle,
          inputDescription, setInputDescription,
          inputYear, setInputYear,
          inputDuration, setInputDuration,
          inputGenre, setInputGenre,
          inputRating, setInputRating,
          indexOfForm, setIndexOfForm,
          idOfList, setIdOfList
        ]
    }>
        {props.children}
    </MovieContext.Provider>
  );
};
