import React, {useContext} from "react"
import axios from "axios";
import { MovieContext } from "./MovieContext";

class TableTitle extends React.Component {
  render() {
    return <h1 className="center">Table of Movies</h1>;
  }
}

class TableHead extends React.Component {
  render() {
    return (
      <thead>
        <tr>
          <th>Title</th>
          <th>Year</th>
          <th>Duration</th>
          <th>Genre</th>
          <th>Rating</th>
          <th>Description</th>
        </tr>
      </thead>
    );
  }
}

const MovieList = () =>{
  const [
    movies, setMovies,
    inputTitle, setInputTitle,
    inputDescription, setInputDescription,
    inputYear, setInputYear,
    inputDuration, setInputDuration,
    inputGenre, setInputGenre,
    inputRating, setInputRating,
    indexOfForm, setIndexOfForm,
    idOfList, setIdOfList
  ] = useContext(MovieContext);

  const handleEdit = (event) =>{
    let index = event.target.value;
    let id = event.target.id;
    let movie = movies[index];

    setInputTitle(movie.title)
    setInputDescription(movie.description)
    setInputYear(movie.year)
    setInputDuration(movie.duration)
    setInputGenre(movie.genre)
    setInputRating(movie.rating)

    setIndexOfForm(index)
    setIdOfList(id);
  }

  const handleDelete = (event) => {
    let index = event.target.value
    let id = event.target.id;
    let newMovies = movies
    newMovies.splice(index, 1)

    setMovies([...newMovies])

    axios.delete(`http://backendexample.sanbercloud.com/api/movies/${id}`)
  }


  return(
    <>
      <TableTitle />
      <table className="margin-center list-table">
        <TableHead />
        <tbody>
          {
            movies.map((val, index)=>{
              return(
                <tr key={index}>
                  <td>{val.title}</td>
                  <td>{val.year}</td>
                  <td>{val.duration}</td>
                  <td>{val.genre}</td>
                  <td className="text-center">{val.rating}</td>
                  <td className="col-45">{val.description}</td>
                  <td className="compact">
                    <button id={val.id} onClick={handleEdit} value={index}>Edit</button>
                  </td>
                  <td className="compact">
                    <button id={val.id} onClick={handleDelete} value={index}>Delete</button>
                  </td>
                </tr>
              )
            })
          }
        </tbody>
      </table>
    </>

  )

}

export default MovieList
