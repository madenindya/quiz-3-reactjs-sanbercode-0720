import React from "react"
import {MovieProvider} from "./MovieContext"
import MovieList from "./MovieList"
import MovieForm from "./MovieForm"

const Movie = () =>{
  return(
    <>
      <section className="col-75 margin-center main-content">
      <MovieProvider>
        <MovieList/>
        <MovieForm/>
      </MovieProvider>
      </section>
    </>
  )
}

export default Movie
