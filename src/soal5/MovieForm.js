import React, {useContext} from "react";
import axios from 'axios';
import { MovieContext} from "./MovieContext";

const FruitForm = () =>{
  const [
    movies, setMovies,
    inputTitle, setInputTitle,
    inputDescription, setInputDescription,
    inputYear, setInputYear,
    inputDuration, setInputDuration,
    inputGenre, setInputGenre,
    inputRating, setInputRating,
    indexOfForm, setIndexOfForm,
    idOfList, setIdOfList
  ] = useContext(MovieContext);


  const handleSubmit = (event) => {
    event.preventDefault()

    const inputMovie = {
      title: inputTitle,
      description: inputDescription,
      year: inputYear,
      duration: inputDuration,
      genre: inputGenre,
      rating: inputRating,
    }

    if (inputTitle.replace(/\s/g,'') !== ""){
      let newMovies = movies;
      let index = indexOfForm;
      let id = idOfList;

      console.log(indexOfForm, id);

      if (index === -1){
        newMovies = [...newMovies, inputMovie];
        axios.post(`http://backendexample.sanbercloud.com/api/movies`,inputMovie)
      }else{
        newMovies[index] = inputMovie;
        axios.put(`http://backendexample.sanbercloud.com/api/movies/${id}`, inputMovie)
      }

      setMovies(newMovies)

      setInputTitle("")
      setInputDescription("")
      setInputYear(0)
      setInputDuration(0)
      setInputGenre("")
      setInputRating(1)

      setIndexOfForm(-1)
      setIdOfList(-1)
    }

  }

  const handleChange = (event) =>{
    const targetName = event.target.name;
    const targetValue = event.target.value;

    if (targetName === "inputTitle") {
      setInputTitle(targetValue);
    } else if (targetName === "inputDescription") {
      setInputDescription(targetValue);
    } else if (targetName === "inputYear") {
      setInputYear(targetValue);
    } else if (targetName === "inputDuration") {
      setInputDuration(targetValue);
    } else if (targetName === "inputGenre") {
      setInputGenre(targetValue);
    } else if (targetName === "inputRating") {
      setInputRating(targetValue);
    }
  }

  return(
    <>
      <h1>Form Input Movie</h1>
      <form onSubmit={handleSubmit}>
        <table className="margin-center">
        <tr>
          <td>Title:</td>
          <td>
            <input
              name="inputTitle"
              type="text"
              value={inputTitle}
              onChange={handleChange}
            />
          </td>
        </tr>
        <tr>
          <td>Year:</td>
          <td>
            <input
              name="inputYear"
              type="number"
              value={inputYear}
              onChange={handleChange}
            />
          </td>
        </tr>
        <tr>
          <td>Duration (minutes):</td>
          <td>
            <input
              name="inputDuration"
              type="number"
              value={inputDuration}
              onChange={handleChange}
            />
          </td>
        </tr>
        <tr>
          <td>Rating:</td>
          <td>
            <input
              name="inputRating"
              type="number"
              value={inputRating}
              onChange={handleChange}
              min="1"
              max="10"
            />
          </td>
        </tr>
        <tr>
          <td>Genre:</td>
          <td>
            <input
              name="inputGenre"
              type="text"
              value={inputGenre}
              onChange={handleChange}
            />
          </td>
        </tr>
        <tr>
          <td>Description:</td>
          <td>
            <textarea rows="10" cols="50"
              name="inputDescription"
              value={inputDescription}
              onChange={handleChange}
            />
          </td>
        </tr>
        </table>
        <div className="text-center">
            <button>submit</button>
        </div>
      </form>
    </>
  )

}

export default FruitForm
