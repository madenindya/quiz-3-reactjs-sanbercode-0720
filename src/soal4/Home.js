import React, { useState, useEffect } from 'react';
import axios from 'axios';

class MovieItem extends React.Component {
    render() {
      return (
        <>
            <h3>{this.props.title}</h3>
            <p>
                <strong>Rating {this.props.rating}</strong><br></br>
                <strong>Durasi: {
                    this.props.duration > 60 ?
                        Math.floor(this.props.duration / 60) + " jam " : ""
                    }{
                    this.props.duration % 60 > 0 ? (
                        this.props.duration % 60) + " menit" : ""
                    }</strong><br></br>
                <strong>genre: {this.props.genre}</strong><br></br>
            </p>
            <p>
                <strong>deskripsi:</strong><br></br>
                {this.props.description}
            </p>
            <hr></hr>
        </>
      )
    }
  }

class MovieList extends React.Component {
    render() {
        return (
            <>
            {
                this.props.movieList.sort((a, b) => {return b.rating - a.rating}).map((val, index) => {
                    return (
                        <MovieItem title={val.title} rating={val.rating} duration={val.duration} genre={val.genre} description={val.description}/>
                    )
                })
            }
            </>
        )
    }
}

const Home = () => {
  const [movieList, setmovieList] =  useState([])

  useEffect(() => {
    if (movieList.length === 0) {
      axios.get(`http://backendexample.sanbercloud.com/api/movies`)
        .then(res => {
          const newData = res.data.map((el) => { return el});
          setmovieList(newData);
        })
    }
  })


  return(
    <>
        <section className="col-75 margin-center main-content">
            <h1>Daftar Film Film Terbaik</h1>
            <MovieList movieList={movieList} />
        </section>
    </>
  )
}

export default Home
