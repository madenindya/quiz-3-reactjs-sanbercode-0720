import React, { useState } from 'react';

class AboutItem extends React.Component {
    render() {
        return (
            <li>
                <strong>{this.props.k}:</strong> {this.props.v}
            </li>
        )
    }
}

const About = () => {
    const [about] =  useState({
        Nama: "Nindya",
        Email: "made.nindyatama@gmail.com",
        "Sistem Operasi yang digunakan": "MacOS 10.13.6",
        "Akun Gitlab": "@madenindya",
        "Akun Telegram": "@madenindya",
    });

    return (
        <>
        <section className="col-75 margin-center main-content">
            <h1>Data Peserta Sanbercode Bootcamp ReactJS</h1>
            <ol>
                {
                    Object.keys(about).map((k) => {
                        return (
                            <AboutItem k={k} v={about[k]}/>
                        )
                    })
                }
            </ol>
        </section>
        </>
    )
}

export default About
