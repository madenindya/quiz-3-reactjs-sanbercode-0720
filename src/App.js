import React from 'react';
import './App.css';
import { BrowserRouter as Router } from "react-router-dom";
import NavLogin from './soal6/NavLogin';
import Routes from './soal3a/Routes';

function App() {
  return (
    <>
    <Router>
      <NavLogin />
      <Routes />
    </Router>
    </>
  );
}

export default App;
