import React from "react";
import {LoginProvider} from "./LoginContext";
import NavLoginList from './NavLoginList';

const NavLogin = () =>{
  return(
    <>
      <LoginProvider>
        <NavLoginList />
      </LoginProvider>
    </>
  )
}

export default NavLogin
