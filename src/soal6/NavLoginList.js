import React, {useContext} from "react"
import {Link} from "react-router-dom";
import {LoginContext} from "./LoginContext";
import logo from './css/logo.png';
import './css/style.css';

const NavColorList = () =>{
  const [
    isLogin, setIsLogin
  ] = useContext(LoginContext);

  const handleLogin = (event) => {
    const id = event.target.id;
    console.log(id);
    if (id === "login") {
        setIsLogin(true);
    } else if (id === "logout") {
        setIsLogin(false);
    }
  }

  return(
    <>
      <div className="nav">
      <ul>
        <Link to="/">
          <img src={logo} width="200px" />
        </Link>
        <li style={{display: (isLogin ? "block": "none")}}>
          <Link to="/" onClick={handleLogin} id="logout">Logout</Link>
        </li>
        <li style={{display: (!isLogin ? "block": "none")}}>
          <Link to="/" onClick={handleLogin} id="login">Login</Link>
        </li>
        <li style={{display: (isLogin ? "block": "none")}}>
          <Link to="/movies">Movie List Editor</Link>
        </li>
        <li>
          <Link to="/about">About</Link>
        </li>
        <li>
          <Link to="/home">Home</Link>
        </li>
      </ul>
    </div>
    </>
  )

}

export default NavColorList
